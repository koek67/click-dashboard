/********************************************************
 *														*
 * 	hotel click summary dashboard		*
 * 	Koushik Krishnan 2nd June 2015 						*
 *														*
 ********************************************************/

/********************************************************
 *														*
 * 	Load data from json file						*
 *														*
 ********************************************************/
d3.json('clicks.json', function(json) {
    /********************************************************
     *														*
     * 	Format the data and initialize global variables
     *														*
     ********************************************************/
    var data = json
    var delay = 700 // time for all transitions (ms)
    var names = []
    data.forEach(function (value) {
        var name = value['true_provider']
        var contain = false;
        // check to see if this primer already exists in 'names'
        names.forEach( function (d) {
            if (d === name)
                contain = true
        })
        if (!contain)
            names.push(name)
    })
    names.sort()
    console.log(data[0]);
    // filter out data that has any null values
    //
    // data.filter(function(d) {
    //
    //     var notNull = 0
    //     notNull += d['cached'] != null
    //     notNull += d['days_to_arrival'] != null
    //     notNull += d['dt'] != null
    //     notNull += d['experiment'] != null
    //     notNull += d['find_match'] != null
    //     notNull += d['length_of_stay'] != null
    //     notNull += d['locale'] != null
    //     notNull += d['measurement_success'] != null
    //     notNull += d['price_diff'] != null
    //     notNull += d['price_match'] != null
    //     notNull += d['true_provider'] != null
    //     return notNull == 11
    // })
    /********************************************************
     *														*
     * 	Create the dc.js chart objects & link to html	*
     *														*
     ********************************************************/
    var barChart = dc.barChart("#bar-chart")
    var losChart = dc.pieChart("#los-chart")
    var localeChart = dc.pieChart("#locale-chart")
    var daysOutChart = dc.pieChart("#days-out")
    var findRateBox = dc.numberDisplay("#find-rate-box");
    var measurementBox = dc.numberDisplay("#measurement-box")
    var priceBox = dc.numberDisplay("#price-box")
    var accuracyBox = dc.numberDisplay("#accuracy-box")
    var overallBox = dc.numberDisplay("#overall-box")

    /********************************************************
     *														*
     * 	Run data through crossfilter				*
     *														*
     ********************************************************/
    var ndx = crossfilter(data)

    /********************************************************
     *														*
     * 	Create Dimensions that we'll need			*
     *														*
     ********************************************************/
    var providerDimension = ndx.dimension(function(d) { return d['true_provider'] })
    var lengthOfStayDimension = ndx.dimension(function(d) {
        var los = d['length_of_stay']
        if (los > 7)
            return ">7"
        else return "<7"
    })
    var localeDimension = ndx.dimension(function(d) { return d['locale'] })
    var daysOutDimension = ndx.dimension(function(d) {
        var daysOut = d['days_to_arrival']
        if (daysOut > 90)
            return ">90"
        else return "<90"
    })

    /********************************************************
     *														*
     * 	Define the grouping and reducing functions for each
     *   dimension.
     *														*
     ********************************************************/
    var providerDimensionGroup = providerDimension.group()
    var lengthOfStayDimensionGroup = lengthOfStayDimension.group()
    var localeDimensionGroup = localeDimension.group()
    var daysOutDimensionGroup = daysOutDimension.group()
    var providerAccuracyGroup = ndx.groupAll().reduce(

        // add
        function(p, v) {
            // officially increment the total_clicks
            //if (v['find_match'] != null)
                p.total_clicks++;
            // calculate the new find rate
            if (v['find_match'] === 1) {
                p.find_rate += v['find_match']
            }

            // calculate the new price rate
            var new_price_rate = 0
            if (v['find_match'] === 1 && (v['price_diff'] >= -10 || v['price_match'] === 1))
                new_price_rate = 1
            p.price_rate += new_price_rate

            // calculate new accuracy rate
            p.accuracy_rate = p.price_rate * p.find_rate

            // calculate measurement success
            if (v['measurement_success'] === 1)
                p.measurement_rate += v['measurement_success']

            // calculate the overall rate
            p.overall_rate = p.find_rate * p.accuracy_rate * p.measurement_rate

            return p;
        },

        // remove
        function(p, v) {
            // officially decrement the total_clicks
            //if (v['find_match'] != null)
                p.total_clicks--;
            // calculate the new find rate
            if (v['find_match'] === 1) {
                p.find_rate -= v['find_match']
            }

            // calculate the new price rate
            var new_price_rate = 0
            if (v['find_match'] === 1 && (v['price_diff'] >= -10 || v['price_match'] === 1))
                new_price_rate = 1
            p.price_rate -= new_price_rate

            // calculate new accuracy rate
            p.accuracy_rate = p.price_rate * p.find_rate

            // calculate measurement success
            if (v['measurement_success'] === 1)
                p.measurement_rate -= v['measurement_success']

            // calculate the overall rate
            p.overall_rate = p.find_rate * p.accuracy_rate * p.measurement_rate

            return p;
        },

        // init
        function(p, v) {
            return {total_clicks: 0, find_rate: 0, price_rate: 0, accuracy_rate: 0, measurement_rate: 0, overall_rate: 0}
        }
    )

    /********************************************************
     *														*
     * 	Create the charts and define each's properties
     *														*
     ********************************************************/
    barChart
        .width(1200)
        .height(600)
        .margins({ top: 10, right: 50, bottom: 150, left: 60 })
        .dimension(providerDimension)
        .group(providerDimensionGroup)
        .x(d3.scale.ordinal().domain(names))
        .elasticY(true)
        .xUnits(dc.units.ordinal)
        .renderHorizontalGridLines(true)
        .renderlet(function(chart) {
            console.log(chart);
            chart.selectAll('g.x text')
                .attr('dx', '-75')
                .attr('dy', '-5')
                .attr('transform', 'rotate(-90)')
        })

    losChart
        .width(200)
        .height(200)
        .dimension(lengthOfStayDimension)
        .group(lengthOfStayDimensionGroup)

    localeChart
        .width(200)
        .height(200)
        .dimension(localeDimension)
        .group(localeDimensionGroup)

    daysOutChart
        .width(200)
        .height(200)
        .dimension(daysOutDimension)
        .group(daysOutDimensionGroup)

    findRateBox
        .formatNumber(d3.format(",%"))
        .valueAccessor(function(d) {
            console.log(d);
            return d['find_rate'] / d['total_clicks'];
        })
        .group(providerAccuracyGroup);

    measurementBox
        .formatNumber(d3.format(",%"))
        .valueAccessor(function(d) {
            console.log(d);
            return d['measurement_rate'] / d['total_clicks'];
        })
        .group(providerAccuracyGroup);

    priceBox
        .formatNumber(d3.format(",%"))
        .valueAccessor(function(d) {
            console.log(d);
            return d['price_rate'] / d['total_clicks'];
        })
        .group(providerAccuracyGroup);

    accuracyBox
        .formatNumber(d3.format(",%"))
        .valueAccessor(function(d) {
            console.log(d);
            return d['accuracy_rate'] / (d['total_clicks'] * d['total_clicks']);
        })
        .group(providerAccuracyGroup);

    overallBox
        .formatNumber(d3.format(",%"))
        .valueAccessor(function(d) {
            console.log(d);
            return d['overall_rate'] / (d['total_clicks'] * d['total_clicks'] * d['total_clicks'] * d['total_clicks']);
        })
        .group(providerAccuracyGroup);

    /********************************************************
     *														*
     * 	Render it all!
     *														*
     ********************************************************/
    dc.renderAll()

})
